import './assets/css/App.css';
import {
  Container
} from "@material-ui/core";
import Home from "./components/Home/Home";
import Navbar from "./components/Navigation/Navigation";
import Navigation from './components/Navigation/Navigation';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';


function App() {
  return (
    <div className="App">
    <Router>
      <Navigation />
      <Switch>
        <Route exact path="/" component={Home} />
        <Container>
          
        </Container>
      </Switch>
    </Router>
    </div>
  );
}

export default App;
