import React, { useState } from "react";
import {
    AppBar,
    Toolbar,
    IconButton,
    Menu, 
    MenuItem,
    Button,
    Typography,
    useMediaQuery
} from "@material-ui/core";
import {
    useTheme,
    makeStyles
} from "@material-ui/core/styles";
import MenuIcon from '@material-ui/icons/Menu';
import {
    Link
} from 'react-router-dom';
import SideBar from './SideBar';



const useStyles = makeStyles(theme => ({
    title: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },
    navigation__bar: {
        backgroundColor: "#1B1E2B"
    },
    navigation__element_container: {
        display: "flex",
        flexDirection: "row",
    },
    button__root: {
        textDecoration: "none",
        color: "white",
        marginLeft: "5px",
        marginRight: "5px",
        backgroundImage: "linear-gradient(to right, #9f21d6, #8f1ec5, #7f1ab5, #7016a4, #611294)"
    },
    button__label: {
        color: "white",
        textTransform: "capitalize"
    }
}));




const navigationSections = [
    "home",
    "blog",
    "portfolio",
    "contact"
];



const Navigation = () => {
    const classes = useStyles();
    const [ openedSideBar, setIsOpenedSideBar ] = useState(false);
    const theme = useTheme();
    const showHMenu = !useMediaQuery(theme.breakpoints.up('sm'));

    const toggleSideBar = (open) => (event) => {
        if (event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
            return;
        }
        setIsOpenedSideBar(open)
    };


    return (
        <AppBar position="sticky" color="default" className={classes.navigation__bar} 
            style={{
                "backgroundColor": "#2E3440"
            }}>
            <Toolbar>
                {
                    showHMenu? (
                        <IconButton 
                            edge="start" 
                            aria-label="menu" 
                            onClick={toggleSideBar(true)}    
                        >
                            <MenuIcon style={{ color: "white" }} />
                        </IconButton>
                    ) : ""
                }
                <SideBar 
                    opened={openedSideBar} 
                    handleSideBar={toggleSideBar} 
                    items={navigationSections} 
                />

                <Typography variant="h4" style={{flexGrow: "1"}}>
                    Sjukdom Kastle
                </Typography>
                
                {
                    !showHMenu? (
                        <div className={classes.navigation__element_container}>
                            { 
                                navigationSections.map((item, key) => (
                                    <Button
                                        key={key}
                                        variant="outlined" 
                                        classes={{
                                            root: classes.button__root,
                                            label: classes.button__label
                                        }}
                                    >
                                        {/* <Link to="/"> { item } </Link> */}
                                        { item }
                                    </Button>
                                ))
                            }
                        </div> 
                    ) : ""
                }
            </Toolbar>
        </AppBar>
    )
};


export default Navigation;