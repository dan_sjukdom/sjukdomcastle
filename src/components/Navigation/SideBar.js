import {
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    Drawer
} from '@material-ui/core';
import {
    makeStyles
} from '@material-ui/core/styles';
// Icons
import HomeIcon from '@material-ui/icons/Home';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import WorkOutlinedIcon from '@material-ui/icons/WorkOutlined';
import ExploreOutlinedIcon from '@material-ui/icons/ExploreOutlined';


const useStyles = makeStyles(theme => ({
    side_bar__paper: {
        backgroundColor: "#2E3440",
    },
    side_bar_list__button: {
        backgroundImage: "linear-gradient(to right, #9f21d6, #8f1ec5, #7f1ab5, #7016a4, #611294)"
    },
    list_item__root: {
        minWidth: "20px",
    },
    list__wrapper: {
        height: "inherit",
    },
    icon: {
        color: "white"
    }
}));



const SideBar = ({opened, handleSideBar, items}) => {
    const classes = useStyles();

    const itemsIcons = [
        <HomeIcon className={classes.icon}/>,
        <ExploreOutlinedIcon className={classes.icon}/>,
        <WorkOutlinedIcon className={classes.icon}/>,
        <ContactMailIcon className={classes.icon}/>
    ];

    return (
        <Drawer
            open={opened} 
            classes={{
                paper: classes.side_bar__paper
            }}
        >
            <div
                role="presentation"
                onClick={handleSideBar(false)}
                onKeyDown={handleSideBar(false)}
                className={classes.list__wrapper}
            >
                {
                    items.map((item, key) => (
                        <List key={key}>
                            <ListItem 
                                button
                                classes={{
                                    button: classes.side_bar_list__button
                                }}
                            >
                                <ListItemIcon 
                                    classes={{
                                        root: classes.list_item__root
                                    }}
                                >
                                    { itemsIcons[key] }
                                </ListItemIcon>
                            </ListItem>
                        </List>
                    ))
                }
            </div>
        </Drawer>
    )
};

export default SideBar;