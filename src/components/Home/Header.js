import { Typography } from '@material-ui/core';
import {
    makeStyles
} from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    container: {
        height: "35vh",
        width: "100%",
        backgroundImage: "linear-gradient(to right, #312d32, #353136, #38343a, #3c383f, #403c43)", 
    },
    header__title: {
        color: "white"
    }
}));

const Header = () => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <Typography variant="h2" className={classes.header__title}>
                Welcome to my castle
            </Typography>
        </div>
    )
};


export default Header;