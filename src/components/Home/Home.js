import React from "react";
import {
    Typography,
    Icon
} from "@material-ui/core";
import {
    makeStyles
} from "@material-ui/core/styles";
// import "../../assets/css/Home.css";
import Header from './Header';


const useStyles = makeStyles(theme => ({
    title: {
        "display": "flex",
        "flexDirection": "column",
        "justifyContent": "center",
        "alignItems": "center",
        "textAlign": "center"
    }
}));




const Home = () => {
    const classes = useStyles();
    return (
        <div>
            <div className={classes.title}>
                <Header />
                <Typography variant="h2" className="text">
                    Welkome to my kastle
                </Typography>
                <Typography component="p" className="text text-secondary">
                    Hello! I'm Daniel Sjukdom, a software developer that loves
                    to code.
                </Typography>
            </div>
        </div>
    )
};


export default Home;